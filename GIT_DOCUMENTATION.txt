					GIT DOCUMENTATION

git init: Questo comando crea un repository vuoto senza commit al suo interno 

git clone: Questo comando permette di clonare un repository

git fetch: Questo comando permette di "prendere" branches o tags da uno o più repository 

git pull: Questo comando aggiorna il repository locale per metterlo in pari con il repository remoto da cui stiamo facendo il pull

git push: Questo comando aggiorna i repository remoti usando i dati del repository locali 

git add: Questo comando permette di aggiungere all'area di preparazione il contenuto trovato nel branch corrente e di prepararlo per il commit. Se lanciato da solo aggiunge tutto, sennò può essere specificato cosa aggiungere all'area di stage

git status: Questo comando permette di visualizzare lo stato del branch corrente, se ci sono modifiche aggiunte all'area di stage e da committare o se ci sono modifiche che non sono ancora state aggiunte.

git branch: Questo comando visualizza i branch esistenti nel repository in cui lavoriamo 

git checkout <NOME BRANCH>: Questo comando permette di spostarsi da un branch ad un altro (se aggiungiamo un "-b" davanti al nome di un branch inesistente, git si occuperà di creare il branch e spostarci lì)

git diff: Questo comando mostra le differenze tra commit, tra due tree e altri oggetti o files 

git commit "<MESSAGGIO>": Questo comando crea un commento contenente l'indice delle modifiche e il messaggio che le descrive. 

git log: Questo comando mostra la storia dei commit partendo dall'ultimo.

git merge <NOME BRANCH>: Questo comando permette di unire il branch specificato nel branch in cui ci troviamo

git stash: Questo comando permette di tenere un record dello stato corrente della directory su cui lavoriamo salvando le modifiche da un'altra parte, facendo ritornare la directory allo stato del commit puntato dall'HEAD

git stash list: Questo comando permette di vedere la lista delle modifiche stashate

git stash apply: Questo comando applica le modifiche stashate che dovranno comunque subire un commit

git apply: Questo comando legge l'output del "git diff" e applica le modifiche ai files 

git rebase: Questo comando permette di cambiare il commit da cui si è creato un branch da quello originale ad un altro

git cherry-pick: Questo comando permette di prendere un commit fatto su un branch e di applicarlo du un altro branch.